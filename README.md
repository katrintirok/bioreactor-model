# Bioreactor NPZ Model

Model to simulate algae, nutrient and grazer dynamics in a chemostat process, i.e. a bioreactor with continous population dilution and renewal of nutrients.  

The jupyter notebook can be viewed [here](https://nbviewer.jupyter.org/urls/gitlab.com/katrintirok/bioreactor-model/raw/master/Bioreactor_NPZ_Model.ipynb).
