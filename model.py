import numpy as np
from scipy import integrate

def NPZmodel(par,initial_cond,start_t,end_t,incr):
    '''
    Function to solve the NPZ model, takes following arguments.
    par: list with parameter values
    initial_cond: list with initial values
    start_t: start time
    end_t: end time
    incr: increments for building time vector 
    returns: list with solutions and time
    '''
    #-time-grid-----------------------------------
    t = np.linspace(start_t, end_t,incr)
    #differential-eq-system----------------------
    def eq(y,t):
        '''
        Function with definition of the differential equations,
        see help for integrate.odeint from module scipy for conventions for arguments
        '''
        N=y[0]  #nutrients
        P=y[1]  #algae
        Z=y[2]  #rotifers
        r0,g,e,d,a,k,Nin = par   
        # the model equations   
        r = r0 * N/(N+k)
        f0 = -a*r*P - d*N + d*Nin
        f1 = r*P - g*P*Z - d*P
        f2 = g*e*P*Z - d*Z
        return [f0, f1, f2]

    #integrate------------------------------------
    ds = integrate.odeint(eq,initial_cond,t)
    return (ds[:,0],ds[:,1],ds[:,2],t)
